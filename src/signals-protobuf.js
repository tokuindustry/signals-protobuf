/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.toku = (function() {

    /**
     * Namespace toku.
     * @exports toku
     * @namespace
     */
    var toku = {};

    toku.domain = (function() {

        /**
         * Namespace domain.
         * @memberof toku
         * @namespace
         */
        var domain = {};

        domain.Slice = (function() {

            /**
             * Properties of a Slice.
             * @memberof toku.domain
             * @interface ISlice
             * @property {number|Long|null} [startMsecs] Slice startMsecs
             * @property {Array.<number>|null} [secondsIncrement] Slice secondsIncrement
             * @property {Array.<number>|null} [reading] Slice reading
             */

            /**
             * Constructs a new Slice.
             * @memberof toku.domain
             * @classdesc Represents a Slice.
             * @implements ISlice
             * @constructor
             * @param {toku.domain.ISlice=} [properties] Properties to set
             */
            function Slice(properties) {
                this.secondsIncrement = [];
                this.reading = [];
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * Slice startMsecs.
             * @member {number|Long} startMsecs
             * @memberof toku.domain.Slice
             * @instance
             */
            Slice.prototype.startMsecs = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

            /**
             * Slice secondsIncrement.
             * @member {Array.<number>} secondsIncrement
             * @memberof toku.domain.Slice
             * @instance
             */
            Slice.prototype.secondsIncrement = $util.emptyArray;

            /**
             * Slice reading.
             * @member {Array.<number>} reading
             * @memberof toku.domain.Slice
             * @instance
             */
            Slice.prototype.reading = $util.emptyArray;

            /**
             * Creates a new Slice instance using the specified properties.
             * @function create
             * @memberof toku.domain.Slice
             * @static
             * @param {toku.domain.ISlice=} [properties] Properties to set
             * @returns {toku.domain.Slice} Slice instance
             */
            Slice.create = function create(properties) {
                return new Slice(properties);
            };

            /**
             * Encodes the specified Slice message. Does not implicitly {@link toku.domain.Slice.verify|verify} messages.
             * @function encode
             * @memberof toku.domain.Slice
             * @static
             * @param {toku.domain.ISlice} message Slice message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Slice.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.startMsecs != null && message.hasOwnProperty("startMsecs"))
                    writer.uint32(/* id 1, wireType 1 =*/9).fixed64(message.startMsecs);
                if (message.secondsIncrement != null && message.secondsIncrement.length) {
                    writer.uint32(/* id 2, wireType 2 =*/18).fork();
                    for (var i = 0; i < message.secondsIncrement.length; ++i)
                        writer.int32(message.secondsIncrement[i]);
                    writer.ldelim();
                }
                if (message.reading != null && message.reading.length) {
                    writer.uint32(/* id 3, wireType 2 =*/26).fork();
                    for (var i = 0; i < message.reading.length; ++i)
                        writer.float(message.reading[i]);
                    writer.ldelim();
                }
                return writer;
            };

            /**
             * Encodes the specified Slice message, length delimited. Does not implicitly {@link toku.domain.Slice.verify|verify} messages.
             * @function encodeDelimited
             * @memberof toku.domain.Slice
             * @static
             * @param {toku.domain.ISlice} message Slice message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Slice.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a Slice message from the specified reader or buffer.
             * @function decode
             * @memberof toku.domain.Slice
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {toku.domain.Slice} Slice
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Slice.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.toku.domain.Slice();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.startMsecs = reader.fixed64();
                        break;
                    case 2:
                        if (!(message.secondsIncrement && message.secondsIncrement.length))
                            message.secondsIncrement = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.secondsIncrement.push(reader.int32());
                        } else
                            message.secondsIncrement.push(reader.int32());
                        break;
                    case 3:
                        if (!(message.reading && message.reading.length))
                            message.reading = [];
                        if ((tag & 7) === 2) {
                            var end2 = reader.uint32() + reader.pos;
                            while (reader.pos < end2)
                                message.reading.push(reader.float());
                        } else
                            message.reading.push(reader.float());
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a Slice message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof toku.domain.Slice
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {toku.domain.Slice} Slice
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Slice.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a Slice message.
             * @function verify
             * @memberof toku.domain.Slice
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Slice.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.startMsecs != null && message.hasOwnProperty("startMsecs"))
                    if (!$util.isInteger(message.startMsecs) && !(message.startMsecs && $util.isInteger(message.startMsecs.low) && $util.isInteger(message.startMsecs.high)))
                        return "startMsecs: integer|Long expected";
                if (message.secondsIncrement != null && message.hasOwnProperty("secondsIncrement")) {
                    if (!Array.isArray(message.secondsIncrement))
                        return "secondsIncrement: array expected";
                    for (var i = 0; i < message.secondsIncrement.length; ++i)
                        if (!$util.isInteger(message.secondsIncrement[i]))
                            return "secondsIncrement: integer[] expected";
                }
                if (message.reading != null && message.hasOwnProperty("reading")) {
                    if (!Array.isArray(message.reading))
                        return "reading: array expected";
                    for (var i = 0; i < message.reading.length; ++i)
                        if (typeof message.reading[i] !== "number")
                            return "reading: number[] expected";
                }
                return null;
            };

            /**
             * Creates a Slice message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof toku.domain.Slice
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {toku.domain.Slice} Slice
             */
            Slice.fromObject = function fromObject(object) {
                if (object instanceof $root.toku.domain.Slice)
                    return object;
                var message = new $root.toku.domain.Slice();
                if (object.startMsecs != null)
                    if ($util.Long)
                        (message.startMsecs = $util.Long.fromValue(object.startMsecs)).unsigned = false;
                    else if (typeof object.startMsecs === "string")
                        message.startMsecs = parseInt(object.startMsecs, 10);
                    else if (typeof object.startMsecs === "number")
                        message.startMsecs = object.startMsecs;
                    else if (typeof object.startMsecs === "object")
                        message.startMsecs = new $util.LongBits(object.startMsecs.low >>> 0, object.startMsecs.high >>> 0).toNumber();
                if (object.secondsIncrement) {
                    if (!Array.isArray(object.secondsIncrement))
                        throw TypeError(".toku.domain.Slice.secondsIncrement: array expected");
                    message.secondsIncrement = [];
                    for (var i = 0; i < object.secondsIncrement.length; ++i)
                        message.secondsIncrement[i] = object.secondsIncrement[i] | 0;
                }
                if (object.reading) {
                    if (!Array.isArray(object.reading))
                        throw TypeError(".toku.domain.Slice.reading: array expected");
                    message.reading = [];
                    for (var i = 0; i < object.reading.length; ++i)
                        message.reading[i] = Number(object.reading[i]);
                }
                return message;
            };

            /**
             * Creates a plain object from a Slice message. Also converts values to other types if specified.
             * @function toObject
             * @memberof toku.domain.Slice
             * @static
             * @param {toku.domain.Slice} message Slice
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Slice.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.arrays || options.defaults) {
                    object.secondsIncrement = [];
                    object.reading = [];
                }
                if (options.defaults)
                    if ($util.Long) {
                        var long = new $util.Long(0, 0, false);
                        object.startMsecs = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                    } else
                        object.startMsecs = options.longs === String ? "0" : 0;
                if (message.startMsecs != null && message.hasOwnProperty("startMsecs"))
                    if (typeof message.startMsecs === "number")
                        object.startMsecs = options.longs === String ? String(message.startMsecs) : message.startMsecs;
                    else
                        object.startMsecs = options.longs === String ? $util.Long.prototype.toString.call(message.startMsecs) : options.longs === Number ? new $util.LongBits(message.startMsecs.low >>> 0, message.startMsecs.high >>> 0).toNumber() : message.startMsecs;
                if (message.secondsIncrement && message.secondsIncrement.length) {
                    object.secondsIncrement = [];
                    for (var j = 0; j < message.secondsIncrement.length; ++j)
                        object.secondsIncrement[j] = message.secondsIncrement[j];
                }
                if (message.reading && message.reading.length) {
                    object.reading = [];
                    for (var j = 0; j < message.reading.length; ++j)
                        object.reading[j] = options.json && !isFinite(message.reading[j]) ? String(message.reading[j]) : message.reading[j];
                }
                return object;
            };

            /**
             * Converts this Slice to JSON.
             * @function toJSON
             * @memberof toku.domain.Slice
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Slice.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return Slice;
        })();

        domain.Envelope = (function() {

            /**
             * Properties of an Envelope.
             * @memberof toku.domain
             * @interface IEnvelope
             * @property {toku.domain.ISlice|null} [slice] Envelope slice
             */

            /**
             * Constructs a new Envelope.
             * @memberof toku.domain
             * @classdesc Represents an Envelope.
             * @implements IEnvelope
             * @constructor
             * @param {toku.domain.IEnvelope=} [properties] Properties to set
             */
            function Envelope(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * Envelope slice.
             * @member {toku.domain.ISlice|null|undefined} slice
             * @memberof toku.domain.Envelope
             * @instance
             */
            Envelope.prototype.slice = null;

            // OneOf field names bound to virtual getters and setters
            var $oneOfFields;

            /**
             * Envelope body.
             * @member {"slice"|undefined} body
             * @memberof toku.domain.Envelope
             * @instance
             */
            Object.defineProperty(Envelope.prototype, "body", {
                get: $util.oneOfGetter($oneOfFields = ["slice"]),
                set: $util.oneOfSetter($oneOfFields)
            });

            /**
             * Creates a new Envelope instance using the specified properties.
             * @function create
             * @memberof toku.domain.Envelope
             * @static
             * @param {toku.domain.IEnvelope=} [properties] Properties to set
             * @returns {toku.domain.Envelope} Envelope instance
             */
            Envelope.create = function create(properties) {
                return new Envelope(properties);
            };

            /**
             * Encodes the specified Envelope message. Does not implicitly {@link toku.domain.Envelope.verify|verify} messages.
             * @function encode
             * @memberof toku.domain.Envelope
             * @static
             * @param {toku.domain.IEnvelope} message Envelope message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Envelope.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.slice != null && message.hasOwnProperty("slice"))
                    $root.toku.domain.Slice.encode(message.slice, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
                return writer;
            };

            /**
             * Encodes the specified Envelope message, length delimited. Does not implicitly {@link toku.domain.Envelope.verify|verify} messages.
             * @function encodeDelimited
             * @memberof toku.domain.Envelope
             * @static
             * @param {toku.domain.IEnvelope} message Envelope message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Envelope.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes an Envelope message from the specified reader or buffer.
             * @function decode
             * @memberof toku.domain.Envelope
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {toku.domain.Envelope} Envelope
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Envelope.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.toku.domain.Envelope();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.slice = $root.toku.domain.Slice.decode(reader, reader.uint32());
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes an Envelope message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof toku.domain.Envelope
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {toku.domain.Envelope} Envelope
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Envelope.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies an Envelope message.
             * @function verify
             * @memberof toku.domain.Envelope
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Envelope.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                var properties = {};
                if (message.slice != null && message.hasOwnProperty("slice")) {
                    properties.body = 1;
                    {
                        var error = $root.toku.domain.Slice.verify(message.slice);
                        if (error)
                            return "slice." + error;
                    }
                }
                return null;
            };

            /**
             * Creates an Envelope message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof toku.domain.Envelope
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {toku.domain.Envelope} Envelope
             */
            Envelope.fromObject = function fromObject(object) {
                if (object instanceof $root.toku.domain.Envelope)
                    return object;
                var message = new $root.toku.domain.Envelope();
                if (object.slice != null) {
                    if (typeof object.slice !== "object")
                        throw TypeError(".toku.domain.Envelope.slice: object expected");
                    message.slice = $root.toku.domain.Slice.fromObject(object.slice);
                }
                return message;
            };

            /**
             * Creates a plain object from an Envelope message. Also converts values to other types if specified.
             * @function toObject
             * @memberof toku.domain.Envelope
             * @static
             * @param {toku.domain.Envelope} message Envelope
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Envelope.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (message.slice != null && message.hasOwnProperty("slice")) {
                    object.slice = $root.toku.domain.Slice.toObject(message.slice, options);
                    if (options.oneofs)
                        object.body = "slice";
                }
                return object;
            };

            /**
             * Converts this Envelope to JSON.
             * @function toJSON
             * @memberof toku.domain.Envelope
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Envelope.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return Envelope;
        })();

        return domain;
    })();

    return toku;
})();

module.exports = $root;
