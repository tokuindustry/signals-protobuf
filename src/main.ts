
/*
 * Copyright (c) 2019 Toku Industry Inc.
 *
 * @Script: main.ts
 * @Author: Leo Chan
 * @Email: leo.chan@tokuindustry.com
 * @Create At: 2019-04-09 13:31:34
 * @Last Modified By: Leo Chan
 * @Last Modified At: 2019-06-13 11:50:55
 * @Description: Encode/decode signals
 */

import * as protobuf from './signals-protobuf';

import compressjs from 'compressjs';

import debugFunc from 'debug';

import base91 from 'node-base91';

function isValidUTF8(bytes: Buffer) {
  let i = 0;
  while (i < bytes.length) {
    if ((// ASCII
      bytes[i] === 0x09 ||
      bytes[i] === 0x0A ||
      bytes[i] === 0x0D ||
      (0x20 <= bytes[i] && bytes[i] <= 0x7E)
    )
    ) {
      i += 1;
      continue;
    }

    if ((// non-overlong 2-byte
      (0xC2 <= bytes[i] && bytes[i] <= 0xDF) &&
      (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0xBF)
    )
    ) {
      i += 2;
      continue;
    }

    if ((// excluding overlongs
      bytes[i] === 0xE0 &&
      (0xA0 <= bytes[i + 1] && bytes[i + 1] <= 0xBF) &&
      (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF)
    ) ||
      (// straight 3-byte
        ((0xE1 <= bytes[i] && bytes[i] <= 0xEC) ||
          bytes[i] === 0xEE ||
          bytes[i] === 0xEF) &&
        (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0xBF) &&
        (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF)
      ) ||
      (// excluding surrogates
        bytes[i] === 0xED &&
        (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0x9F) &&
        (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF)
      )
    ) {
      i += 3;
      continue;
    }

    if ((// planes 1-3
      bytes[i] === 0xF0 &&
      (0x90 <= bytes[i + 1] && bytes[i + 1] <= 0xBF) &&
      (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF) &&
      (0x80 <= bytes[i + 3] && bytes[i + 3] <= 0xBF)
    ) ||
      (// planes 4-15
        (0xF1 <= bytes[i] && bytes[i] <= 0xF3) &&
        (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0xBF) &&
        (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF) &&
        (0x80 <= bytes[i + 3] && bytes[i + 3] <= 0xBF)
      ) ||
      (// plane 16
        bytes[i] === 0xF4 &&
        (0x80 <= bytes[i + 1] && bytes[i + 1] <= 0x8F) &&
        (0x80 <= bytes[i + 2] && bytes[i + 2] <= 0xBF) &&
        (0x80 <= bytes[i + 3] && bytes[i + 3] <= 0xBF)
      )
    ) {
      i += 4;
      continue;
    }

    return false;
  }

  return true;
}

const debug = debugFunc('illumass:signals');

export enum Encoding {
  Binary = 1,
  Base64 = 2,
  Base91 = 3,
}

export type Envelope = protobuf.toku.domain.IEnvelope;
export type Slice = protobuf.toku.domain.ISlice;

export enum CompressionAlgorithm {
  None = 1,
  LZP3,
}

interface Codec {
  compressFile: (buf: Uint8Array) => Uint8Array;
  decompressFile: (buf: Uint8Array) => Uint8Array;
}

function compressionAlgorithm(alg?: CompressionAlgorithm) {
  switch (alg || CompressionAlgorithm.LZP3) {
    case CompressionAlgorithm.None:
      return {
        compressFile: (buf: Buffer | Uint8Array) => {
          return buf;
        },
        decompressFile: (buf: Buffer | Uint8Array) => {
          return buf;
        },
      };

    case CompressionAlgorithm.LZP3:
      return compressjs.Lzp3;

    default:
      throw new Error('Compression algorithm ' + alg + ' is not supported');
  }
}

export function encode(envelope: Envelope, encoding?: Encoding, alg?: CompressionAlgorithm): Buffer {
  const codec = compressionAlgorithm(alg);
  const start = Date.now();

  const message = protobuf.toku.domain.Envelope.create(envelope);
  const buffer = Buffer.from(protobuf.toku.domain.Envelope.encode(message).finish());
  const protobufElapsed = Date.now() - start;
  debug('Protobuf encoding time: %d size: %d', protobufElapsed, buffer.length);
  const compressed = Buffer.from(codec.compressFile(buffer));
  const compressElapsed = Date.now() - start - protobufElapsed;
  debug('Compression time: %d size: %d (%d%%)',
    compressElapsed, compressed.length, Math.round((compressed.length / buffer.length) * 100));

  switch (encoding || Encoding.Binary) {
    case Encoding.Binary:
      return compressed;
    case Encoding.Base64:
      const base64Buffer = Buffer.from(compressed.toString('base64'));
      const base64Elapsed = Date.now() - start - compressElapsed;
      debug('Base64 encoding time: %d size: %d (%d%%)',
        base64Elapsed, base64Buffer.length, Math.round((base64Buffer.length / buffer.length) * 100));
      return base64Buffer;
    case Encoding.Base91:
      const base91Buffer = Buffer.from(base91.encode(compressed));
      const base91Elapsed = Date.now() - start - compressElapsed;
      debug('Base91 encoding time: %d size: %d (%d%%)',
        base91Elapsed, base91Buffer.length, Math.round((base91Buffer.length / buffer.length) * 100));
      return base91Buffer;
    default: return compressed;
  }
}

export function decode(buf: Buffer, encoding?: Encoding, alg?: CompressionAlgorithm): Envelope {
  const codec = compressionAlgorithm(alg);

  let compressed: Buffer;
  const start = Date.now();
  let firstDecodeElapsed = 0;

  switch (encoding || Encoding.Binary) {
    case Encoding.Binary:
      compressed = buf;
      break;

    case Encoding.Base64:
      if (!isValidUTF8(buf)) {
        throw new Error('buffer must be a valid utf8 encoded string.');
      }

      compressed = Buffer.from(buf.toString(), 'base64');
      firstDecodeElapsed = Date.now() - start;
      debug('Base64 decoding time: %d size: %d (%d%%)',
        firstDecodeElapsed, compressed.length, Math.round((compressed.length / buf.length) * 100));
      break;

    case Encoding.Base91:
      if (!isValidUTF8(buf)) {
        throw new Error('buffer must be a valid utf8 encoded string.');
      }

      compressed = base91.decode(buf.toString());
      firstDecodeElapsed = Date.now() - start;
      debug('Base91 decoding time: %d size: %d (%d%%)',
        firstDecodeElapsed, compressed.length, Math.round((compressed.length / buf.length) * 100));
      break;

    default:
      compressed = Buffer.alloc(0);
      break;
  }

  const decompressed = Buffer.from(codec.decompressFile(compressed));
  const decompressedElapsed = Date.now() - start - firstDecodeElapsed;
  debug('Decompression time: %d size: %d (%d%%)',
    decompressedElapsed, decompressed.length, Math.round((decompressed.length / buf.length) * 100));

  const env = protobuf.toku.domain.Envelope.decode(decompressed);
  const protobufElapsed = Date.now() - start - decompressedElapsed;
  debug('Protobuf decoding time: %d', protobufElapsed);

  return env;
}

export function decodeBase91(buffer: any) {
  return base91.decode(buffer);
}
