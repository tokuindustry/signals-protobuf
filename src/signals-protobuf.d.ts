import * as Long from 'long';
import * as $protobuf from "protobufjs";
/** Namespace toku. */
export namespace toku {

    /** Namespace domain. */
    namespace domain {

        /** Properties of a Slice. */
        interface ISlice {

            /** Slice startMsecs */
            startMsecs?: (number|Long|null);

            /** Slice secondsIncrement */
            secondsIncrement?: (number[]|null);

            /** Slice reading */
            reading?: (number[]|null);
        }

        /** Represents a Slice. */
        class Slice implements ISlice {

            /**
             * Constructs a new Slice.
             * @param [properties] Properties to set
             */
            constructor(properties?: toku.domain.ISlice);

            /** Slice startMsecs. */
            public startMsecs: (number|Long);

            /** Slice secondsIncrement. */
            public secondsIncrement: number[];

            /** Slice reading. */
            public reading: number[];

            /**
             * Creates a new Slice instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Slice instance
             */
            public static create(properties?: toku.domain.ISlice): toku.domain.Slice;

            /**
             * Encodes the specified Slice message. Does not implicitly {@link toku.domain.Slice.verify|verify} messages.
             * @param message Slice message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: toku.domain.ISlice, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Slice message, length delimited. Does not implicitly {@link toku.domain.Slice.verify|verify} messages.
             * @param message Slice message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: toku.domain.ISlice, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Slice message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Slice
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): toku.domain.Slice;

            /**
             * Decodes a Slice message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Slice
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): toku.domain.Slice;

            /**
             * Verifies a Slice message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Slice message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Slice
             */
            public static fromObject(object: { [k: string]: any }): toku.domain.Slice;

            /**
             * Creates a plain object from a Slice message. Also converts values to other types if specified.
             * @param message Slice
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: toku.domain.Slice, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Slice to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an Envelope. */
        interface IEnvelope {

            /** Envelope slice */
            slice?: (toku.domain.ISlice|null);
        }

        /** Represents an Envelope. */
        class Envelope implements IEnvelope {

            /**
             * Constructs a new Envelope.
             * @param [properties] Properties to set
             */
            constructor(properties?: toku.domain.IEnvelope);

            /** Envelope slice. */
            public slice?: (toku.domain.ISlice|null);

            /** Envelope body. */
            public body?: "slice";

            /**
             * Creates a new Envelope instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Envelope instance
             */
            public static create(properties?: toku.domain.IEnvelope): toku.domain.Envelope;

            /**
             * Encodes the specified Envelope message. Does not implicitly {@link toku.domain.Envelope.verify|verify} messages.
             * @param message Envelope message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: toku.domain.IEnvelope, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Envelope message, length delimited. Does not implicitly {@link toku.domain.Envelope.verify|verify} messages.
             * @param message Envelope message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: toku.domain.IEnvelope, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an Envelope message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Envelope
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): toku.domain.Envelope;

            /**
             * Decodes an Envelope message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Envelope
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): toku.domain.Envelope;

            /**
             * Verifies an Envelope message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an Envelope message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Envelope
             */
            public static fromObject(object: { [k: string]: any }): toku.domain.Envelope;

            /**
             * Creates a plain object from an Envelope message. Also converts values to other types if specified.
             * @param message Envelope
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: toku.domain.Envelope, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Envelope to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }
    }
}
