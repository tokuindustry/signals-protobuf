/*
 * Copyright (c) 2019 Toku Industry Inc.
 *
 * @Script: protobuf.spec.ts
 * @Author: Leo Chan
 * @Email: leo.chan@tokuindustry.com
 * @Create At: 2019-04-09 13:31:19
 * @Last Modified By: Leo Chan
 * @Last Modified At: 2019-04-14 16:07:20
 * @Description: Test encoding/deconding signals
 */

import 'jasmine';
import * as _ from 'lodash';
import Long from 'long';
import * as signals from './main';

describe('Signals Protobuf', () => {

  function envelope(start: Date): signals.Envelope {
    return {
      slice: {
        reading: [100.1, 200.2, 300.3, 400.4, 500.5],
        secondsIncrement: [1, 2, 3, 4, 5],
        startMsecs: start.valueOf(),
      },
    };
  }

  function checkEnvelope(start: Date, env: signals.Envelope) {
    expect(env.slice).toBeTruthy();
    if (env.slice) {
      if (env.slice.startMsecs && env.slice.startMsecs instanceof Long) {
        expect(new Date(env.slice.startMsecs.toNumber())).toEqual(start);
        if (env.slice.secondsIncrement && env.slice.reading) {
          expect(env.slice.secondsIncrement.length).toEqual(env.slice.reading.length);
          expect(env.slice.secondsIncrement[0]).toEqual(1);
          expect(env.slice.secondsIncrement[1]).toEqual(2);
          expect(env.slice.secondsIncrement[2]).toEqual(3);
          expect(env.slice.secondsIncrement[3]).toEqual(4);
          expect(env.slice.secondsIncrement[4]).toEqual(5);

          expect(Math.abs(env.slice.reading[0] - 100.1)).toBeLessThan(0.0001);
          expect(Math.abs(env.slice.reading[1] - 200.2)).toBeLessThan(0.0001);
          expect(Math.abs(env.slice.reading[2] - 300.3)).toBeLessThan(0.0001);
          expect(Math.abs(env.slice.reading[3] - 400.4)).toBeLessThan(0.0001);
          expect(Math.abs(env.slice.reading[4] - 500.5)).toBeLessThan(0.0001);
        } else {
          fail('secondsIncrement and/or reading are missing.');
        }
      } else {
        fail('startMsecs unexpected type.');
      }
    } else {
      fail('slice missing.');
    }
  }

  it('should encode and decode single signal', () => {
    const now = new Date();
    now.setMilliseconds(0);

    _.forEach(
      _.filter(
        _.map(Object.keys(signals.Encoding), (k) => Number(k)),
        (k) => !Number.isNaN(k)),
      (k) => {
        const encoding: signals.Encoding = (k as signals.Encoding);
        const buf = signals.encode(envelope(now), encoding);
        const env = signals.decode(buf, encoding);

        checkEnvelope(now, env);
      });
  });

  it('should encode and decode single signal without compression', () => {
    const now = new Date();
    now.setMilliseconds(0);

    _.forEach(
      _.filter(
        _.map(Object.keys(signals.Encoding), (k) => Number(k)),
        (k) => !Number.isNaN(k)),
      (k) => {
        const encoding: signals.Encoding = (k as signals.Encoding);
        const buf = signals.encode(envelope(now), encoding, signals.CompressionAlgorithm.None);
        const env = signals.decode(buf, encoding, signals.CompressionAlgorithm.None);

        checkEnvelope(now, env);
      });
  });

});
