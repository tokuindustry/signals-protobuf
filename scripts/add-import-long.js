const path = require('path');
const fs = require('fs');

const filePath = process.argv[2];

const line = Buffer.from("import * as Long from 'long';\n");
const buf = fs.readFileSync(filePath);

const modified = Buffer.concat([line, buf]);

fs.writeFileSync(filePath, modified);