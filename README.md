# Signals Protobuf

This package encodes and decodes signal data. Starting from 3.1.0, es6 code is emitted into *./lib/*. It is up to the user of the library to emit out into usable form. (via babel).

## Scripts

* `npm run clean`
  Clean artifacts

* `npm run build`
  Build the project. It creates two variants
  * `lib` contains ES6/ES2015 module

## Release procedure

1. `git flow release start x.y.z`
1. `npm --no-git-tag-version version <major|minor|patch>`
1. `git commit -a -m 'Bump version'`
1. `git flow release finish x.y.z`
1. `git push`
1. `git checkout master`
1. `git push`
1. `git push --tags`
1. `nvm use`
1. `npm publish --access public`
    npm will ask for a OTP.
