const path = require('path')
const webpack = require('webpack')

const babelTypescriptOptions = {
  presets: [
    '@babel/typescript',
    [
      '@babel/env',
      {
        modules: 'commonjs',
        targets: {
          node: true
        }
      }
    ]
  ],
  plugins: [
    '@babel/proposal-class-properties',
    '@babel/proposal-object-rest-spread'
  ]
}
const babelWebOptions = {
  presets: [
    '@babel/typescript',
    [
      '@babel/env',
      {
        modules: 'commonjs',
        targets: 'defaults, IE 11',
      }
    ]
  ],
  plugins: [
    '@babel/proposal-class-properties',
    '@babel/proposal-object-rest-spread'
  ]
}
module.exports = [
  {
    mode: 'development',
    target: 'web',
    entry: {
      main: './lib/main.js'
    },
    node: {
      fs: 'empty',
      vm: 'empty',
      module: 'empty',
      path: 'empty',
      url: true
    },
    output: {
      filename: '[name].amd.js',
      path: path.join(__dirname, 'dist'),
      libraryTarget: 'commonjs'
    },
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /src\/.+\.ts$/,
          exclude: /(node_modules|\.d\.ts$)/,
          use: {
            loader: 'babel-loader',
            options: babelWebOptions
          }
        },
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: 'defaults, IE 11',
                    modules: 'commonjs'
                  }
                ]
              ]
            }
          }
        },
        {
          test: /\/node_modules\/compressjs\/.*\.js$/,
          use: {
            loader: 'string-replace-loader',
            options: {
              multiple: [
                { search: 'if (typeof define !== \'function\') { var define = require(\'amdefine\')(module); }', replace: '' },
                { search: '    BWTC = Object.create(null);', replace: '    var BWTC = Object.create(null);' }
              ]
            }
          }
        },

      ]
    },
    resolve: {
      alias: {
        'node-base91': path.resolve(__dirname, 'node_modules', 'node-base91', 'index.js'),
        'debug': path.resolve(__dirname, 'node_modules', 'debug', 'dist', 'debug.js'),
      },
      extensions: ['.js', '.ts', '.json', '.wasm']
    },
    plugins: [
      new webpack.IgnorePlugin(/^(http|min-req-promise|package|ws)$/)
    ],
    externals: {
      'utf-8-validate': 'utf-8-validate'
    }
  },
  {
    mode: 'development',
    target: 'node',
    entry: {
      main: './lib/main.js'
    },
    output: {
      filename: '[name].js',
      path: path.join(__dirname, 'dist'),
      libraryTarget: 'commonjs2'
    },
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /src\/.+\.ts$/,
          exclude: /(node_modules|\.d\.ts$)/,
          use: {
            loader: 'babel-loader',
            options: babelTypescriptOptions
          }
        },
        {
          test: /\.js$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: {
                      node: true
                    },
                    modules: false
                  }
                ]
              ]
            }
          }
        },
        {
          test: /\/node_modules\/compressjs\/.*\.js$/,
          use: {
            loader: 'string-replace-loader',
            options: {
              multiple: [
                { search: 'if (typeof define !== \'function\') { var define = require(\'amdefine\')(module); }', replace: '' },
                { search: '    BWTC = Object.create(null);', replace: '    var BWTC = Object.create(null);' }
              ]
            }
          }
        },

      ]
    },
    resolve: {
      alias: {
        'node-base91': path.resolve(__dirname, 'node_modules', 'node-base91', 'index.js'),
        'debug': path.resolve(__dirname, 'node_modules', 'debug', 'dist', 'debug.js'),
      },
      extensions: ['.js', '.ts', '.json', '.wasm']
    },
    externals: {
      'utf-8-validate': 'utf-8-validate'
    }
  }
]