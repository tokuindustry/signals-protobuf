/* eslint-disable semi */
module.exports = function (api) {
  api.cache(true);

  const presets = [
    '@babel/typescript'
  ];

  const plugins = [
    '@babel/plugin-transform-runtime',
    '@babel/proposal-class-properties',
    '@babel/proposal-object-rest-spread'
  ];

  return {
    presets,
    plugins
  };
};
